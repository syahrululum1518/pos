@extends('templates/main')

@section('css')
    <style>

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="mt-3">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex justify-content-between">
                            <div class="container">
                                <div class="row d-flex align-items-center">
                                    <div class="col text-left" style="text-align:left;">
                                        <h4 style="text-align:left;">Daftar Kategori Barang</h4>
                                    </div>
                                    <div class="col" style="text-align:right;">
                                        <a href="{{ url('/manage_product/category/create') }}" class="btn btn-primary">
                                            <span>Tambah Kategori</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="row d-flex justify-content-center align-items-center">
                            <div class="col-12 grid-margin ">
                                <div class="iq-card">
                                    <div class="iq-card-body">
                                        <div class="table-responsive-xl" style="overflow: scroll;">
                                            <table
                                                class="table table-hover table-striped table-light display sortable text-nowrap"
                                                cellspacing="0" id="myTable">
                                                <thead>
                                                    <br>
                                                    <tr id="_judul" onkeyup="_filter()" id="myFilter">
                                                        <th>ID</th>
                                                        <th>Kategori</th>
                                                        <th></th>
                                                        {{-- <th></th> --}}
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach ($categories as $key => $category)
                                                        <tr>
                                                            <td>{{ $key + 1 }}</td>
                                                            <td>{{ $category->nama_kategori }}</td>
                                                            <td width="20%">
                                                                <button type="button" class="btn btn-sm btn-warning"
                                                                    onclick="location.href='{{ url('/manage_product/category/' . $category->id . '/edit') }}'">
                                                                    <span><i class="fa fa-edit"></i>Edit</span>
                                                                </button>
                                                                <button type="button" class="btn btn-sm btn-danger"
                                                                    data-toggle="modal"
                                                                    data-target="#deleteDaftarCategory.{{ $category->id }}">
                                                                    <span><i class="fa fa-trash"></i>Delete</span>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        <div class="modal fade"
                                                            id="deleteDaftarCategory.{{ $category->id }}" role="dialog"
                                                            style="border-radius:45px">
                                                            <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <form
                                                                        action="{{ url('/manage_product/category/' . $category->id . '/delete') }}"
                                                                        method="POST">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <div class="modal-header"
                                                                            style="background:rgba(52, 25, 80, 1); color:white;">
                                                                            <p id="employeeidname"
                                                                                style="font-weight: bold;">
                                                                                DELETE {{ $category->nama_kategori }}
                                                                            </p>
                                                                            <button type="button" class="close"
                                                                                data-dismiss="modal"
                                                                                style="color:white;">×</button>
                                                                        </div>

                                                                        <div class="modal-body">
                                                                            <div class="row text-center">
                                                                                <div class="col">
                                                                                    Apakah Yakin Menghapus
                                                                                    {{ $category->nama_kategori }}?
                                                                                </div>

                                                                            </div>
                                                                            <br>
                                                                            <div class="row text-center">
                                                                                <div class="col">
                                                                                    <button
                                                                                        class="btn btn-secondary text-right"
                                                                                        data-dismiss="modal"
                                                                                        style="text-align:left">
                                                                                        <a style="color: white;">Cancel</a>
                                                                                    </button>
                                                                                    <button
                                                                                        class="btn btn-danger text-right"
                                                                                        style="text-align:left">
                                                                                        <a style="color: white;">Delete</a>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                        </div>
                                        @endforeach
                                        </tbody>
                                        </table>
                                        <div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('script')
        <script>
            @if ($message = Session::get('create_success'))
                swal(
                    "Berhasil!",
                    "{{ $message }}",
                    "success"
                );
            @endif

            @if ($message = Session::get('update_success'))
                swal(
                    "Berhasil!",
                    "{{ $message }}",
                    "success"
                );
            @endif

            @if ($message = Session::get('delete_success'))
                swal(
                    "Berhasil!",
                    "{{ $message }}",
                    "success"
                );
            @endif

            @if ($message = Session::get('delete_error'))
                swal(
                    "Hapus Kategori Gagal!",
                    "{{ $message }}",
                    "error"
                );
            @endif

            $(document).ready(function() {
                $('#myTable').DataTable({
                    "oSearch": {
                        "bSmart": false,
                        "bRegex": true
                    },
                });
            });
        </script>
    @endsection
